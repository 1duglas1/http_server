const { Writable } = require('stream');

class HttpResponse extends Writable {
  constructor(socket) {
    super();
    this.socket = socket;
    this.headers = {};
    this.body = Buffer.alloc(0);
    this.code = 200;
    this.getStartString = () => `HTTP/1.0 ${this.code}`;

    this.sended = false;

    this.on('finish', () => {
      this.socket.end(Buffer.from('\r\n'));
    });
  }

  getStringHeaders() {
    return Object.keys(this.headers)
      .map(key => `${key}: ${this.headers[key]}`)
      .join('\r\n');
  }

  setHeader(headerName, val) {
    this.headers[headerName] = val;
  }

  setStatus(status) {
    this.code = status;
  }

  writeHead(status = null) {
    if (status) this.code = status;

    const startedString = Buffer.from(this.getStartString() + '\r\n', 'utf8');
    this.socket.write(startedString);
  }

  sendHeaders(cb) {
    if (this.sended) cb(new Error('Хедеры уже отправлены'));

    this.sended = true;
    const headers = this.getStringHeaders();
    if (headers) this.socket.write(Buffer.from(headers + '\r\n\r\n', 'utf8'));
    else this.socket.write(Buffer.from('\r\n'));
  }

  _write(chunk, enc, next) {
    if (!this.sended) {
      this.writeHead();
      this.sendHeaders(next);
    }

    try {
      this.socket.write(chunk);
      next();
    } catch (e) {
      next(e);
    }
  }
}

module.exports = HttpResponse;
