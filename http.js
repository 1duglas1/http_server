const net = require('net');
const HttpRequest = require('./HttpRequest');
const HttpResponse = require('./HttpResponse');
const HttpServer = require('./HttpServer');
const HelperStream = require('./HelperStream');

const httpServer = HttpServer.createServer(socket => {
  const breakBuf = Buffer.from('\r\n\r\n');

  let headers = Buffer.from('');

  const req = new HttpRequest(socket);
  const res = new HttpResponse(socket);
  const helper = new HelperStream({ req, socket });

  const handler = buf => {
    const isFullHeaders = buf.includes(breakBuf);

    if (isFullHeaders) {
      const indexBreak = buf.indexOf(breakBuf);

      const body = buf.slice(indexBreak + breakBuf.length);
      console.log('body', body.toString());
      headers = Buffer.from(headers + buf.slice(0, indexBreak));

      socket.removeListener('data', handler);
      socket.pause();
      req.parseHeaders(headers);
      httpServer.emit('request', req, res);
      socket.unshift(body);
      socket.pipe(helper);
    } else {
      headers = Buffer.from(headers + buf);
    }
  };

  socket.on('data', handler);
});

module.exports = {
  createServer: () => httpServer,
};
