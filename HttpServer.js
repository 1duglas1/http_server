const net = require('net');

const { EventEmitter } = require('events');
class HttpServer extends EventEmitter {
  constructor(cb) {
    super();
    this.server = net.createServer(cb);
  }

  static createServer(cb) {
    return new HttpServer(cb);
  }

  listen(port, cb) {
    return this.server.listen(
      {
        host: 'localhost',
        port: process.env.PORT || 3000,
        exclusive: true,
      },
      cb,
    );
  }
}

module.exports = HttpServer;
