const http = require('./http');
const fs = require('fs');

const server = http.createServer();

server.listen(3000, () => {
  console.log('server is listen');
});

server.on('request', (req, res) => {
  console.log(req.headers, req.method, req.url);
  const pathToFile = __dirname + '/static' + req.url;
  const pathToNotFound = __dirname + '/static/NotFound.html';
  const s = fs.createReadStream(pathToFile);

  s.on('error', e => {
    console.log(e);
    switch (e.code) {
      case 'EISDIR':
        res.setStatus(400);
        res.end();
        break;
      case 'ENOENT':
        res.setStatus(404);
        fs.createReadStream(pathToNotFound).pipe(res);
        break;
      default:
        res.setStatus(500);
        res.end();
        break;
    }
  });

  s.on('readable', () => s.pipe(res));
});
