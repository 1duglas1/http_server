const { Readable } = require('stream');

class HttpRequest extends Readable {
  constructor(socket) {
    super();
    this.socket = socket;
    this.headers = {};
    this.method = undefined;
    this.url = undefined;
  }

  _read(data) {
    this.socket.resume();
  }

  /**
   * Функция парсит хедеры, метод и путь и записывает их в класс
   *
   * { headers, method, url } = this
   * @property {Object }headers
   * @param {Buffer} headers
   * @memberof HttpRequest
   */
  parseHeaders(headers) {
    const headersArr = headers.toString('utf8').split('\r\n');
    const startedString = headersArr.shift();

    this.method = startedString.split(' ')[0];
    this.url = startedString.split(' ')[1];

    headersArr.forEach(data => {
      const [key, val] = data.split(': ');
      this.headers[key.toLowerCase()] = val;
    });
  }
}

module.exports = HttpRequest;
