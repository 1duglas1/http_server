const RN = Buffer.from('\r\n');

/**
 * Разделяет строку запроса на хедеры, тело и сообщает, полный ли запрос
 * @param {Buffer} buf Строка запроса
 * @returns {[Buffer, Buffer, Boolean]} [headers, body, isEnd] -> [Хедеры, Тело, Полный ли запрос]
 */
function getHeadersBody(buf) {
  const i = buf.indexOf(RN + RN);
  const headers = buf.slice(0, i);
  const body = buf.slice(i + 4);
  let isEnd = body.includes(RN);

  if (buf.includes(RN + RN)) {
    if (!body.length) isEnd = true;
    return [headers, body, isEnd];
  } else return [headers, Buffer.from(''), false];
}

module.exports = getHeadersBody;
