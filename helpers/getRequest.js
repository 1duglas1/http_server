const { EOL } = require('os');

module.exports = function getRequest(str) {
  const request = {};
  const parsed = str.split('\r\n\r\n');
  const headers = parsed[0].split('\r\n');
  const startedString = headers.shift();
  // const body = parsed[1];

  request.method = startedString.split(' ')[0];
  request.url = startedString.split(' ')[1];

  request.headers = {};
  headers.forEach(data => {
    const [key, val] = data.split(' ');

    request.headers[key] = val;
  });

  return request;
};
