const { Writable } = require('stream');

module.exports = class HelperStream extends Writable {
  constructor({ req, socket }) {
    super();
    this.req = req;
    this.socket = socket;
    this.contentLength = null;
    this.pushedContentLength = 0;
  }

  _write(buff, enc, next) {
    if (!this.contentLength) this.contentLength = parseInt(this.req.headers['content-length']);

    this.socket.pause();
    this.req.push(buff);
    this.pushedContentLength += buff.length;

    if (this.pushedContentLength >= this.contentLength) this.req.push(null);

    next();
    return this.req;
  }
};
